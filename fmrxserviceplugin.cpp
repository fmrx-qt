#include <QMediaServiceProvider>

#include "fmrxserviceplugin.h"
#include "fmrxservice.h"

QStringList FmRxServicePlugin::keys() const
{
	return QStringList() << QLatin1String(Q_MEDIASERVICE_RADIO);
}

QMediaService* FmRxServicePlugin::create(const QString &key)
{
	if (key == QLatin1String(Q_MEDIASERVICE_RADIO))
		return new FmRxService;

	return 0;
}

void FmRxServicePlugin::release(QMediaService *service)
{
	delete service;
}

Q_EXPORT_PLUGIN2(fmrx-qt, FmRxServicePlugin)
