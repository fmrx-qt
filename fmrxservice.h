#ifndef FMRXSERVICE_H
#define FMRXSERVICE_H

#include <QAudio>
#include <QMediaService>
#include <QRadioTuner>

struct FmRxPriv;
class FmRxProxy;
class FmRxControl;

class FmRxService : public QMediaService
{
    Q_OBJECT

public:
    explicit FmRxService(QObject *parent = 0);
	~FmRxService();

	QMediaControl *requestControl(const char *name);
	void releaseControl(QMediaControl *control);

	void start();
	void stop();

	bool isAvailable() const;
	QtMultimediaKit::AvailabilityError availabilityError() const;

	bool isActive() const;

	double frequency();
	void setFrequency(double frequency);

	void searchForward();
	void searchBackward();

	ushort signalLevel() const;

signals:
	void tuned(double frequency);
	void started();
	void stopped();
	void signalLevelChanged(ushort level);
	void piReceived(ushort pi);
	void psReceived(const QString &ps);
	void rtReceived(const QString &rt);

private slots:
	void handleTuned(double frequency);
	void handleStopped();
	void handleSignalLevelChanged(ushort level);

private:
	FmRxPriv *d;
};

#endif // FMRXSERVICE_H
