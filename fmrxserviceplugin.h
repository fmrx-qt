#ifndef FMRXSERVICEPLUGIN_H
#define FMRXSERVICEPLUGIN_H

#include <QMediaServiceProviderPlugin>

class FmRxServicePlugin : public QMediaServiceProviderPlugin {
    Q_OBJECT

public:
	QStringList keys() const;
	QMediaService* create(QString const& key);
	void release(QMediaService *service);
};

#endif // FMRXSERVICEPLUGIN_H
