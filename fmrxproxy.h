#ifndef FMRXPROXY_H
#define FMRXPROXY_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <dbus/dbus.h>

/** This is the Proxy class that connects to the D-Bus service. */

class FmRxProxy : public QObject
{
    Q_OBJECT
public:
    explicit FmRxProxy(QObject *parent = 0);
	~FmRxProxy();

	int Connect();
	void Tune(double frequency);
	void SearchBackward();
	void SearchForward();

signals:
	void Tuned(double frequency);
	void Stopped();
	void SignalLevelChanged(ushort level);
	void PiReceived(ushort pi);
	void PsReceived(const QString &ps);
	void RtReceived(const QString &rt);

private:
	DBusConnection *m_conn;

	static DBusHandlerResult bus_message_filter(DBusConnection *conn,
		DBusMessage *m, void *user_data);

};

#endif // FMRXPROXY_H
