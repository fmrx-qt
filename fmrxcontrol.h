#ifndef FMRXCONTROL_H
#define FMRXCONTROL_H

#include <QRadioTunerControl>

class FmRxService;

class FmRxControl : public QRadioTunerControl
{
    Q_OBJECT
private:
	FmRxService *m_service;

public:
	explicit FmRxControl(FmRxService *parent = 0);

	bool isAvailable() const;
	QtMultimediaKit::AvailabilityError availabilityError() const;

	QRadioTuner::State state() const;

	QRadioTuner::Band band() const;
	void setBand(QRadioTuner::Band b);
	bool isBandSupported(QRadioTuner::Band b) const;

	int frequency() const;
	int frequencyStep(QRadioTuner::Band b) const;
	QPair<int, int> frequencyRange(QRadioTuner::Band b) const;
	void setFrequency(int frequency);

	bool isStereo() const;
	QRadioTuner::StereoMode stereoMode() const;
	void setStereoMode(QRadioTuner::StereoMode mode);

	int signalStrength() const;

	int volume() const;
	void setVolume(int volume);

	bool isMuted() const;
	void setMuted(bool muted);

	bool isSearching() const;
	void cancelSearch();

	void searchForward();
	void searchBackward();

	void start();
	void stop();

	QRadioTuner::Error error() const;
	QString errorString() const;

private:
	static int convertSignalLevel(int v4l_level);

private slots:
	void handleStarted();
	void handleStopped();
	void handleTuned(double frequency);
	void handleSignalLevelChanged(ushort level);
};

#endif // FMRXCONTROL_H
