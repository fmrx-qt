#include <QtCore/QFile>
#include <QtDebug>

#include "fmrxcontrol.h"
#include "fmrxproxy.h"
#include "fmrxthread.h"
#include "fmrxservice.h"

struct FmRxPriv
{
	FmRxThread *thread;
	FmRxProxy *proxy;
	FmRxControl *control;
	bool active;
	double frequency;
	quint16 signalLevel;
};

FmRxService::FmRxService(QObject *parent) :
    QMediaService(parent)
{
	d = new FmRxPriv;
	d->thread = new FmRxThread(this);
	d->proxy = new FmRxProxy(this);
	d->control = new FmRxControl(this);
	d->active = false;

	connect(d->proxy, SIGNAL(Tuned(double)), this, SLOT(handleTuned(double)));
	connect(d->proxy, SIGNAL(Stopped()), this, SLOT(handleStopped()));
	connect(d->proxy, SIGNAL(SignalLevelChanged(ushort)), this, SLOT(handleSignalLevelChanged(ushort)));
	connect(d->proxy, SIGNAL(PiReceived(ushort)), this, SIGNAL(piReceived(ushort)));
	connect(d->proxy, SIGNAL(PsReceived(QString)), this, SIGNAL(psReceived(QString)));
	connect(d->proxy, SIGNAL(RtReceived(QString)), this, SIGNAL(rtReceived(QString)));
}

FmRxService::~FmRxService()
{
	delete d;
}

QMediaControl *FmRxService::requestControl(const char *name)
{
	if (qstrcmp(name, QRadioTunerControl_iid) == 0)
		return d->control;

	return 0;
}

void FmRxService::releaseControl(QMediaControl *control)
{
	// Nothing to do; the control instance will be free when the service is freed
}

void FmRxService::start()
{
	if (d->thread->isRunning()) {
		qWarning("FmRxService already started");
		return;
	}
	d->thread->setInputFd(d->proxy->Connect());
	d->thread->start();
}

void FmRxService::stop()
{
	d->thread->stop();
}

bool FmRxService::isAvailable() const
{
	// TODO Check avaibility of fmrxd service
	return true;
}

QtMultimediaKit::AvailabilityError FmRxService::availabilityError() const
{
	return QtMultimediaKit::NoError;
}

bool FmRxService::isActive() const
{
	return d->active;
}

void FmRxService::handleTuned(double frequency)
{
	if (!qFuzzyCompare(d->frequency, frequency)) {
		d->frequency = frequency;
		emit tuned(frequency);
	}
	if (!d->active) {
		d->active = true;
		emit started();
	}
}

void FmRxService::handleStopped()
{
	if (d->active) {
		d->active = false;
		emit stopped();
	}
}

void FmRxService::handleSignalLevelChanged(ushort level)
{
	d->signalLevel = level;
	if (d->active) {
		emit signalLevelChanged(level);
	}
}

double FmRxService::frequency()
{
	return d->frequency;
}

void FmRxService::setFrequency(double frequency)
{
	d->proxy->Tune(frequency);
}

void FmRxService::searchForward()
{
	d->proxy->SearchForward();
}

void FmRxService::searchBackward()
{
	d->proxy->SearchBackward();
}

ushort FmRxService::signalLevel() const
{
	return d->signalLevel;
}
