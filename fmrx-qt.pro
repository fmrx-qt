#-------------------------------------------------
#
# Project created by QtCreator 2011-12-30T19:56:22
#
#-------------------------------------------------

QT += core
QT -= gui

TARGET = fmrx-qt
TEMPLATE = lib
CONFIG += plugin mobility link_pkgconfig
MOBILITY += multimedia
PKGCONFIG += dbus-glib-1 libpulse

contains(MEEGO_EDITION,harmattan) {
	target.path = /usr/lib/qt4/plugins/mediaservice
} else {
	target.path = $$[QT_INSTALL_PLUGINS]/mediaservice
}
INSTALLS += target

SOURCES += fmrxserviceplugin.cpp \
    fmrxservice.cpp \
    fmrxcontrol.cpp \
    fmrxproxy.cpp \
    fmrxthread.cpp

HEADERS += fmrxserviceplugin.h \
    fmrxservice.h \
    fmrxcontrol.h \
    fmrxproxy.h \
    fmrxthread.h

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog
