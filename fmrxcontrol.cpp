#include "fmrxcontrol.h"
#include "fmrxservice.h"

FmRxControl::FmRxControl(FmRxService *parent) :
	QRadioTunerControl(parent), m_service(parent)
{
	connect(m_service, SIGNAL(started()), this, SLOT(handleStarted()));
	connect(m_service, SIGNAL(stopped()), this, SLOT(handleStopped()));
	connect(m_service, SIGNAL(tuned(double)), this, SLOT(handleTuned(double)));
	connect(m_service, SIGNAL(signalLevelChanged(ushort)), this, SLOT(handleSignalLevelChanged(ushort)));
}

bool FmRxControl::isAvailable() const
{
	return m_service->isAvailable();
}

QtMultimediaKit::AvailabilityError FmRxControl::availabilityError() const
{
	return m_service->availabilityError();
}

QRadioTuner::State FmRxControl::state() const
{
	return m_service->isActive() ?
		QRadioTuner::ActiveState : QRadioTuner::StoppedState;
}

QRadioTuner::Band FmRxControl::band() const
{
	return QRadioTuner::FM;
}

void FmRxControl::setBand(QRadioTuner::Band b)
{
	// Only one band!
	Q_UNUSED(b);
}

bool FmRxControl::isBandSupported(QRadioTuner::Band b) const
{
	return b == QRadioTuner::FM;
}

int FmRxControl::frequency() const
{
	return m_service->frequency() * 1000000.0;
}

int FmRxControl::frequencyStep(QRadioTuner::Band b) const
{
	Q_UNUSED(b); // Only one band!
	return 100 * 1000; // 0.1 Mhz
}

QPair<int, int> FmRxControl::frequencyRange(QRadioTuner::Band b) const
{
	if (b == QRadioTuner::FM)
		return qMakePair<int,int>(87500000, 108000000);

	return qMakePair<int,int>(0, 0);
}

void FmRxControl::setFrequency(int frequency)
{
	m_service->setFrequency(frequency / 1000000.0);
}

bool FmRxControl::isStereo() const
{
	// The driver does not support reporting this
	return true;
}

QRadioTuner::StereoMode FmRxControl::stereoMode() const
{
	return QRadioTuner::Auto;
}

void FmRxControl::setStereoMode(QRadioTuner::StereoMode mode)
{
	// TODO
}

int FmRxControl::signalStrength() const
{
	return convertSignalLevel(m_service->signalLevel());
}

int FmRxControl::volume() const
{
	return 100; // See comment in setVolume
}
void FmRxControl::setVolume(int volume)
{
	// It is IMHO best not to have a separate volume control in Harmattan.
	Q_UNUSED(volume);
}

bool FmRxControl::isMuted() const
{
	return false;
}

void FmRxControl::setMuted(bool muted)
{
	// Set comment in setVolume
	Q_UNUSED(muted);
}

bool FmRxControl::isSearching() const
{
	// Searches are quite fast.
	return false;
}

void FmRxControl::cancelSearch()
{
	// I am not sure this driver supports cancelling a search.
	qWarning("FmRxControl: cannot cancel a search");
}

void FmRxControl::searchForward()
{
	m_service->searchForward();
}

void FmRxControl::searchBackward()
{
	m_service->searchBackward();
}

void FmRxControl::start()
{
	m_service->start();
}

void FmRxControl::stop()
{
	m_service->stop();
}

QRadioTuner::Error FmRxControl::error() const
{
	return QRadioTuner::NoError;
}

QString FmRxControl::errorString() const
{
	return QString();
}

int FmRxControl::convertSignalLevel(int v4l_level)
{
	return (v4l_level * 100) / 65535;
}

void FmRxControl::handleStarted()
{
	emit stateChanged(QRadioTuner::ActiveState);
}

void FmRxControl::handleStopped()
{
	emit stateChanged(QRadioTuner::StoppedState);
}

void FmRxControl::handleTuned(double frequency)
{
	emit frequencyChanged(frequency * 1000000.0);
}

void FmRxControl::handleSignalLevelChanged(ushort level)
{
	emit signalStrengthChanged(convertSignalLevel(level));
}
