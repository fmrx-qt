#ifndef FMRXTHREAD_H
#define FMRXTHREAD_H

#include <QThread>

#include <pulse/pulseaudio.h>

class FmRxThread : public QThread
{
	Q_OBJECT
public:
	explicit FmRxThread(QObject *parent = 0);

	void setInputFd(int fd);
	int inputFd() const;

	bool error() const;

	void run();
	void stop();

signals:

private:
	bool configure();
	void deconfigure();
	bool loop();

private:
	bool m_quit;
	bool m_error;
	int m_fd;
	pa_mainloop *m_mainloop;
	pa_context *m_context;
	pa_stream *m_stream;
};

#endif // FMRXTHREAD_H
